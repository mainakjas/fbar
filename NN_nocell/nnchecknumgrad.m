function nnchecknumgrad(nn, x, y)
    epsilon = 1e-6;
    er = 1e-7;
    n = nn.n;
    for l = 1 : (n - 1)
        if l == 1
            sw = size(nn.W1);
        elseif l == 2
            sw = size(nn.W2);
        end
        for i = 1 : sw(1)
            for j = 1 : sw(2)
                if l == 1
                    nn_m = nn; nn_p = nn;
                    nn_m.W1(i,j) = nn.W1(i,j) - epsilon;
                    nn_p.W1(i,j) = nn.W1(i,j) + epsilon;
                    rng(0);
                    nn_m = nnff(nn_m, x, y);
                    rng(0);
                    nn_p = nnff(nn_p, x, y);
                    dW = (nn_p.L - nn_m.L) / (2 * epsilon);
                    e = abs(dW - nn.dW1(i,j));
                elseif l == 2
                    nn_m = nn; nn_p = nn;
                    nn_m.W2(i,j) = nn.W2(i,j) - epsilon;
                    nn_p.W2(i,j) = nn.W2(i,j) + epsilon;
                    rng(0);
                    nn_m = nnff(nn_m, x, y);
                    rng(0);
                    nn_p = nnff(nn_p, x, y);
                    dW = (nn_p.L - nn_m.L) / (2 * epsilon);
                    e = abs(dW - nn.dW2(i,j));
                end
                assert(e < er, 'numerical gradient checking failed');
            end
        end
    end
end
