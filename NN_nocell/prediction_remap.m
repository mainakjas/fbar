function pred_y = prediction_remap(y)

pred_y = ones(size(y));

% c2 = find(y==2);
% c3 = find(y==3);

pred_y(y==2) = -1;
pred_y(y==3) = 0;