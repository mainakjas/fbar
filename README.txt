Filter-Bank Artifact Detection for EEG

This is a MATLAB toolbox for single-channel EEG artifact detection in offline or real-time settings. 

This is currently Version 0.9 - Pre-release version. I am still planning on updating code, cleaning things up, and adding some functionaity. 


Please see the publication introducing this toolbox:

If you use this toolbox for your research, please cite the publication above. 