function cleanEEG = runModel(EEG)
Fs = 220;
step = 22;
nWins = floor((size(EEG,1)-Fs)/step)-10;

% Load model, filters, and feature stats
load ArtifactModel
filters = initFilters('All',Fs);
selected = Model.ftrStats.selected;
ftrmean = Model.ftrStats.mean;
ftrstd = Model.ftrStats.std;

% Window EEG and detect artifacts
EEGwin = zeros(Fs,4,nWins);
artifacts = zeros(nWins,4);
for i = 1:nWins
    for ch = 1:4
        s = (i-1)*step+1;
        f = s+Fs-1;
        data = EEG(s:f,ch);
        data = data-mean(data);
        EEGwin(:,ch,i) = data;
        ftrs = extractSelectedFtrs(data,Fs,filters,selected,ftrmean,ftrstd);
        artifacts(i,ch) = svmclassify(Model,ftrs');
    end
end

% Convert back to time series
artifactTS = zeros(size(EEG));
for ch = 1:4
    artifactTS(1:Fs,ch) = artifacts(1,ch);
    for i = 2:nWins-1
        s = (i-1)*step+1;
        f = s+Fs;
        artifactTS(s:f,ch) = artifacts(i,ch);
    end
end

% Output clean EEG per channel
cleanEEG = cell(1,4);
CleanWins = cell(1,4);
nSamps = size(artifactTS,1);
for ch = 1:4
    temp = EEG(1:nSamps,ch);
    temp(artifactTS(:,ch)==1) = [];
    cleanEEG{ch} = temp;
    CleanWins{ch} = squeeze(EEGwin(:,ch,artifacts(:,ch)==0));
end


% Plot Results
IXDATA = convert2IXDATA(EEG);
save temp IXDATA
plotYtimes(dataset,artifactTS');
delete temp




    