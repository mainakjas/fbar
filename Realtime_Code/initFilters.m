function [filters] = initFilters(type,Fs)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Returns a struct containing the coefficients of the bandpass filter bank
% used in either EOG or EMG artifact detection for the Muse.
%
% USAGE:
%   filters = initArtifactFilters(type)
%   
%   Output:
%       filters: struct containing arrays of filter coefficients for each
%       bandpass filter.
%
%   Input:
%       type: 'EOG' or 'EMG'
%       Fs (numerical): sampling frequency
%
% Written by Kiret Dhindsa
% Last update: Nov 24, 2015
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
switch type
    case 'EOG'
        [b1,a1] = butter(3,[1/(0.5*Fs),48/(0.5*Fs)]);
        [b2,a2] = butter(3,[0.5/(0.5*Fs),8/(0.5*Fs)]);
        [b3,a3] = butter(3,[8/(0.5*Fs),13/(0.5*Fs)]);
        [b4,a4] = butter(3,[13/(0.5*Fs),18/(0.5*Fs)]);
        [b5,a5] = butter(3,[18/(0.5*Fs),25/(0.5*Fs)]);
        filters.filt1.a = a1;
        filters.filt1.b = b1;
        filters.filt2.a = a2;
        filters.filt2.b = b2;
        filters.filt3.a = a3;
        filters.filt3.b = b3;
        filters.filt4.a = a4;
        filters.filt4.b = b4;
        filters.filt5.a = a5;
        filters.filt5.b = b5;
    case 'EMG'
        [b1,a1] = butter(3,[30/(0.5*Fs),45/(0.5*Fs)]);
        [b2,a2] = butter(3,[65/(0.5*Fs),80/(0.5*Fs)]);
        [b3,a3] = butter(3,[80/(0.5*Fs),100/(0.5*Fs)]);
        filters.filt1.a = a1;
        filters.filt1.b = b1;
        filters.filt2.a = a2;
        filters.filt2.b = b2;
        filters.filt3.a = a3;
        filters.filt3.b = b3;
    case 'All'
        [b1,a1] = butter(3,[0.5/(0.5*Fs),4/(0.5*Fs)]);
        [b2,a2] = butter(3,[4/(0.5*Fs),8/(0.5*Fs)]);
        [b3,a3] = butter(3,[8/(0.5*Fs),13/(0.5*Fs)]);
        [b4,a4] = butter(3,[13/(0.5*Fs),18/(0.5*Fs)]);
        [b5,a5] = butter(3,[18/(0.5*Fs),25/(0.5*Fs)]);
        [b6,a6] = butter(3,[25/(0.5*Fs),30/(0.5*Fs)]);
        [b7,a7] = butter(3,[30/(0.5*Fs),45/(0.5*Fs)]);
        [b8,a8] = butter(3,[65/(0.5*Fs),80/(0.5*Fs)]);
        [b9,a9] = butter(3,[80/(0.5*Fs),100/(0.5*Fs)]);
        filters.filt1.a = a1;
        filters.filt1.b = b1;
        filters.filt2.a = a2;
        filters.filt2.b = b2;
        filters.filt3.a = a3;
        filters.filt3.b = b3;
        filters.filt4.a = a4;
        filters.filt4.b = b4;
        filters.filt5.a = a5;
        filters.filt5.b = b5;
        filters.filt6.a = a6;
        filters.filt6.b = b6;
        filters.filt7.a = a7;
        filters.filt7.b = b7;
        filters.filt8.a = a8;
        filters.filt8.b = b8;
        filters.filt9.a = a9;
        filters.filt9.b = b9;
end

