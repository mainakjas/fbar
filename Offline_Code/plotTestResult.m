function plotTestResult(Model,datafile,opt)
if ~iscell(datafile); datafile = {datafile}; end

nD = length(datafile);





if opt.plot.testVlabels && opt.classify.crossval
    for d = 1:nD
        [IXDATA,Y] = feval([datafile{d},'_ManualLabels'],opt.data.chan);
        X = IXDATA.raw.eeg.data;
        T = length(X);
        Ytrue = getYtimes(Model.Ytrue,T,opt);
        Ytest = getYtimes(Model.Yhat,T,opt);
        
        
        
        %% Output of Current Model versus Labeled Data
        h(d) = figure;
        
        % Identify samples which do not belong to relevant classes
        rmclass = setxor(class,[2,3,4]);
        rmtimes = zeros(length(Ytimes),1);
        for rm = rmclass
            rmtimes = rmtimes|Y(rm,:)';
        end
        rmtimes = Y(1,:)|Y(class,:);
        
        % Plot Classifier output for MLP and APP version
        sh2(1) = subplot(4,1,1);
        goodraw = X; goodraw(Ytimes==1) = nan;
        badraw = X; badraw(Ytimes==0) = nan;
        plot(TS-TS(1),goodraw,'b');
        hold on;
        plot(TS-TS(1),badraw,'r')
        title('Model Classification Output')
        
        sh2(2) = subplot(4,1,2);
        goodlabels = X; goodlabels(Y(class,:)==1) = nan;
        badlabels = X; badlabels(Y(class,:)==0) = nan;
        rmlabels = X; rmlabels(rmtimes,:) = nan;
        plot(TS-TS(1),goodlabels,'b');
        hold on;
        plot(TS-TS(1),badlabels,'r')
        plot(TS-TS(1),rmlabels,'c')
        title('True Labels')
        
        % Plot FFTs
        if length(size(Xfft)) == 2
            Xfft_model = log10(mean(Xfft(:,Yhat==1),2));
            Xfft_true = log10(mean(Xfft(:,testlabels==0),2));
            Xfft_mean = log10(mean(Xfft,2));
        elseif length(size(Xfft)) == 3
            Xfft_model = log10(mean(Xfft(:,:,Yhat==1),3));
            Xfft_true = log10(mean(Xfft(:,:,testlabels==0),3));
            Xfft_mean = log10(mean(Xfft,3));
        end
        Fr = linspace(0,FS/2,size(Xfft,1));
        
        sh2(3) = subplot(4,1,3:4);
        plot(Fr,Xfft_mean(:,1:length(chan)),'k')
        hold on
        plot(Fr,Xfft_model(:,1:length(chan)),'b')
        hold on
        plot(Fr,Xfft_true(:,1:length(chan)),'g')
        legend('Total','Model','True')
        title('FFTs')
        linkaxes(sh2(1:2),'xy')
        
        Result.fft_corr = corr(Xfft_model,Xfft_true);
        fprintf('FFT Correlation: %g\n',Result.fft_corr);
        
        if opt.plot.barfft
            figure;
            bar(Fr,Xfft_mean,1,'b','BaseValue',-4); hold on
            bar(Fr,Xfft_model,1,'g','BaseValue',-4);
        end
        
    end
end