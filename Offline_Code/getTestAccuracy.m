
nChan = length(Result);
acc = zeros(nChan,1);
auc = zeros(nChan,1);
fnr = zeros(nChan,1);
fpr = zeros(nChan,1);
for ch = 1:nChan
    % Get model labels and true labels
    yhat = Result{ch}.Ytimes{ch};
    ytrue = Result{ch}.Ytrue{ch}(opt.classify.class,:);
     
    % Ignore data from other classes
    if opt.classify.class ~= 1
        keepidx = Result{ch}.Ytrue{ch}(1,:)|Result{ch}.Ytrue{ch}(opt.classify.class,:);
        ytrue = ytrue(keepidx);
        yhat = yhat(keepidx);
    end
    
    % Compute performance metrics
    acc(ch) = mean(ytrue==yhat);
    [~,~,~,auc(ch)] = perfcurve(ytrue,yhat,1);
    [~,~,~,per] = confusion(ytrue,yhat);
    fnr(ch) = per(2,1);
    fpr(ch) = per(2,2);
end