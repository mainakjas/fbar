function deleteTestFtrs(DATASETS)

nDS = length(DATASETS);
[f,~,~] = fileparts(which('artifacts1.mat'));
cd(f)
for ds = 1:nDS
    origvars = who('-file',DATASETS{ds});
    load(DATASETS{ds})
    clear test*
    keepvars = origvars(~strncmpi(origvars,'test',4));
    save(DATASETS{ds},keepvars{:});
end
    
    