function [FTRS,LABS,FFTS] = getTrainingData(DATASETS,opt)
% Extracts training features, labels, and FFTS for given datasets and
% options. Computes feature sets if not available, or loads them if
% previously computed and stored.

nD = length(DATASETS);
FTRS = cell(nD,1);
LABS = cell(nD,1);
FFTS = cell(nD,1);
for ds = 1:nD
%     fprintf('----Obtaining Features for Dataset %g----\n',ds);
    eval(sprintf('clear ftrs_%s fft_%s labels_%s',...
        opt.features.type,opt.features.type,opt.features.type));
    load(DATASETS{ds})
    if (~exist(['ftrs_',opt.features.type],'var'))||(~exist(['labels_',opt.features.type],'var'))||(~exist(['fft_',opt.features.type],'var'))
        % Create feature variable and save for later use
        eval(sprintf('ftrs_%s=cell(4,1);',opt.features.type));
        save(DATASETS{ds},['ftrs_',opt.features.type],'-append');
        eval(sprintf('fft_%s=cell(4,1);',opt.features.type));
        save(DATASETS{ds},['fft_',opt.features.type],'-append');
        eval(sprintf('labels_%s=cell(4,1);',opt.features.type));
        save(DATASETS{ds},['labels_',opt.features.type],'-append');
    end
    for chan = opt.data.chan
        if eval(sprintf('length(ftrs_%s)<chan',opt.features.type));
            % In case the data cells are too small
            eval(sprintf('ftrs_%s{chan}=[];',opt.features.type));
            eval(sprintf('fft_%s{chan}=[];',opt.features.type));
            eval(sprintf('labels_%s{chan}=[];',opt.features.type));
        end
        if isempty(eval(['ftrs_',opt.features.type,'{chan}']))||isempty(eval(['labels_',opt.features.type,'{chan}']))||isempty(eval(['fft_',opt.features.type,'{chan}']))
            % Compute and save features if they don't already exist
            [ftrtemp,labtemp,ffttemp] = extractFTRS(DATASETS{ds},chan,opt);
            eval(sprintf('ftrs_%s{chan}=ftrtemp;',opt.features.type));
            eval(sprintf('fft_%s{chan}=ffttemp;',opt.features.type));
            eval(sprintf('labels_%s{chan}=labtemp;',opt.features.type));
            save(DATASETS{ds},['ftrs_',opt.features.type],'-append');
            save(DATASETS{ds},['fft_',opt.features.type],'-append');
            save(DATASETS{ds},['labels_',opt.features.type],'-append');
            if opt.classify.combinechans > 0
                FTRS{ds} = [FTRS{ds},ftrtemp];
                LABS{ds} = [LABS{ds},labtemp];
                FFTS{ds} = [FFTS{ds},ffttemp];
            else
                FTRS{ds} = ftrtemp;
                LABS{ds} = labtemp;
                FFTS{ds} = ffttemp;
            end
        else
            % Extract relevant features, ffts, and labels
            if opt.classify.combinechans > 0
                FTRS{ds} = [FTRS{ds},eval(sprintf('ftrs_%s{chan}',opt.features.type))];
                LABS{ds} = [LABS{ds},eval(sprintf('labels_%s{chan}',opt.features.type))];
                FFTS{ds} = [FFTS{ds},eval(sprintf('fft_%s{chan}',opt.features.type))];
            else
                FTRS{ds} = eval(sprintf('ftrs_%s{chan}',opt.features.type));
                LABS{ds} = eval(sprintf('labels_%s{chan}',opt.features.type));
                FFTS{ds} = eval(sprintf('fft_%s{chan}',opt.features.type));
            end
        end
    end
    % Switch to FASTER or FASTER+ labels if option is set
    if strcmpi(opt.classify.labels,'faster')
        load('FASTER_results','FasterLabels');
        LABS = FasterLabels;
    elseif strcmpi(opt.classify.labels,'faster+')
        load('FASTER_plus_results2','LABELS')
        [~,FS] = fixTS(IXDATA.raw.eeg.times(1:end));
        nW = length(LABS{ds})/4;
        for ch = 1:4
            templabels = zeros(1,nW);
            for i = 1:nW
                str = 1+(i-1)*floor(opt.window.step*FS);
                fin = str+floor(opt.window.winlen*FS)-1;
                templabels(i) = mean(LABELS{ds}(ch,str:fin))>opt.classify.labelcutoff;
            end
            LABS{ds}(1+(ch-1)*nW:ch*nW) = -(templabels-1);
        end
    end
end
