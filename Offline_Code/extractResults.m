%%
nT = length(Model);
[nS,nK] = size(Model{1});
nP = length(Model{1}(1,1).WinAcc);

Result = cell(nT,1);
for t = 1:nT
    Result{t}.WinAcc = zeros(nS,nK,nP);
    Result{t}.TSAcc = zeros(nS,nK,nP);
    Result{t}.AUC = zeros(nS,nK,nP);
    Result{t}.FNR = zeros(nS,nK,nP);
    Result{t}.FPR = zeros(nS,nK,nP);
    for s = 1:nS
        for k = 1:nK
            Result{t}.WinAcc(s,k,:) = Model{t}(s,k).WinAcc;
            Result{t}.TSAcc(s,k,:) = Model{t}(s,k).TSacc;
            Result{t}.AUC(s,k,:) = Model{t}(s,k).AUC;
            for p = 1:nP
                Result{t}.FNR(s,k,p) = Model{t}(s,k).performance{p}(2,1);
                Result{t}.FPR(s,k,p) = Model{t}(s,k).performance{p}(2,2);
            end
        end
    end
end
            
%%    
nT = length(Model);
[nS,nK] = size(Model{1});

Result = cell(nT,1);
for t = 1:nT
    Result{t}.WinAcc = zeros(nS,nK);
    Result{t}.TSAcc = zeros(nS,nK);
    Result{t}.AUC = zeros(nS,nK);
    Result{t}.FNR = zeros(nS,nK);
    Result{t}.FPR = zeros(nS,nK);
    for s = 1:nS
        for k = 1:nK
            Result{t}.WinAcc(s,k) = Model{t}(s,k).WinAcc;
            Result{t}.TSAcc(s,k) = Model{t}(s,k).TSacc;
            Result{t}.AUC(s,k) = Model{t}(s,k).AUC;
        end
    end
end

%%
nT = length(Result);
[nS,nK] = size(Result{1});
nP = length(Result{1}(1,1).WinAcc);

R = cell(nT,1);
for t = 1:nT
    R{t}.WinAcc = zeros(nS,nK,nP);
    R{t}.TSAcc = zeros(nS,nK,nP);
    R{t}.AUC = zeros(nS,nK,nP);
    R{t}.FNR = zeros(nS,nK,nP);
    R{t}.FPR = zeros(nS,nK,nP);
    for s = 1:nS
        for k = 1:nK
            R{t}.WinAcc(s,k,:) = Result{t}(s,k).WinAcc;
            R{t}.TSAcc(s,k,:) = Result{t}(s,k).TSacc;
            R{t}.AUC(s,k,:) = Result{t}(s,k).AUC;
            for p = 1:nP
                R{t}.FNR(s,k,p) = Result{t}(s,k).performance{p}(2,1);
                R{t}.FPR(s,k,p) = Result{t}(s,k).performance{p}(2,2);
            end
        end
    end
end