%% Data and Feature Plots
% Plot raw training data
if opt.plot.raw
    figure;
    shraw = zeros(1,length(chan));
    for c = 1:length(chan)
        shraw(c) = subplot(length(chan),1,chan(c));
        good = IXDATA.raw.eeg.data(:,chan(c));
        good(Y(class,:)==1) = nan;
        bad = IXDATA.raw.eeg.data(:,chan(c));
        bad(Y(class,:)==0) = nan;
        plot(good,'b');
        hold on;
        plot(bad,'r')
        if c == 1
            title(['Trianing set ',DATASETS{ds},' raw data'])
        end
    end
    linkaxes(shraw,'x');
end

% Plot Raw data in windows
if opt.plot.windows
    % Prep data for plots
    Xdata = DATA.cat_windowed{class};
    Xdata = squeeze(Xdata);
    labels = LABELS{class};
    L = double(unique(labels));
    Dims = size(Xdata);
    nDims = length(Dims);
    Xwindow = cell(length(L),1);
    Xwindow_mean = cell(length(L),1);
    hwindow_mean = figure;
    if opt.window.step == 1
        hwindow_conc = figure;
    end
    for l = 1:length(L)
        % Separate class data
        if nDims == 2
            Xwindow{l} = Xdata(:,labels==L(l));
            DimsWin = size(Xwindow{l});
            Xcatplot = reshape(Xwindow{l},1,DimsWin(1)*DimsWin(2));
        elseif nDims == 3
            Xwindow{l} = Xdata(:,:,labels==L(l));
            DimsWin = size(Xwindow{l});
            Xcatplot = reshape(Xwindow{l},DimsWin(1),DimsWin(2)*DimsWin(3));
        end
        Xwindow_mean{l} = mean(Xwindow{l},nDims);
        % Plot average class window
        figure(hwindow_mean)
        for c = 1:length(chan)
            subplot(length(L),length(chan),(l-1)*(length(chan))+c);
            if nDims == 2
                plot(Xwindow_mean{l});
            elseif nDims == 3
                plot(Xwindow_mean{l}(c,:));
            end
        end
        % Plot concatenated windows per class
        if opt.window.step == 1
            figure(hwindow_conc)
            for c = 1:length(chan)
                subplot(length(L),length(chan),(l-1)*(length(chan))+c);
                if nDims == 2
                    plot(Xcatplot);
                elseif nDims == 3
                    plot(Xcatplot(c,:));
                end
            end
        end
    end
end

% Plot Features
if opt.plot.features
    % Prep feature data for plotting
    Xftrs = FTRS{class};
    labels = LABELS{class};
    L = double(unique(labels));
    Xplot = cell(length(L),1);
    Xplot_mean = cell(length(L),1);
    hftr_mean = figure;
    legendVec = zeros(1,length(L));
    legendLabels = cell(1,length(L));
    c = {'b','m'};
    % Separate class data and plot
    for l = 1:length(L)
        Xplot{l} = Xftrs(:,labels==L(l));
        Xplot_mean{l} = mean(Xplot{l},2);
        Xplot_std{l} = std(Xplot{l},[],2);
        % Plot average class window
        figure(hftr_mean)
        plot(Xplot_mean{l},c{l},'LineWidth',2); hold on;
        hh = shadedErrorBar(1:size(Xftrs,1),Xplot_mean{l},Xplot_std{l},c{l},1); hold on;
        legendVec(l) = hh.patch;
        if length(L) == 2
            if l > 1; break; end
            legendLabels{1} = 'Clean';
            legendLabels{2} = 'Artifacts';
        else
            legendLabels{l} = ['Class ',num2str(l)];
        end
    end
    xlabel('Features')
    ylabel('Mean Value')
    title('Feature Distributions')
    legend(legendVec,legendLabels)
end

% Plot FBCSP features in their typical form
if opt.plot.features && strcmp(opt.features.type,'fbcsp')
    Xftrs = FTRS{class};
    nComps = opt.features.cspcomps;
    nBands = nFtrs/nComps;
    bandfactors = factor(nBands);               %%% FIXME - need to generalize subplot size by checking multiples of floor(squrt(nBands))
    if length(bandfactors) == 2
        sp1 =  bandfactors(1);
        sp2 = bandfactors(2);
    end
    hfbcsp = figure;
    for b = 1:nBands
        bandInds = (b-1)*nComps+1:b*nComps;
        subplot(sp1,sp2,b)
        plot(mean(Xftrs(bandInds(1:nComps/2),:),2),'b'); hold on;
        plot(mean(Xftrs(bandInds(nComps/2+1:nComps),:),2),'m');
    end
end