function viewIXDATA(dataset)
load(dataset)

sh = zeros(4,1);
figure;
for chan = 1:4
    sh(chan) = subplot(4,1,chan);
    dat = IXDATA.raw.eeg.data(:,chan);
    plot(dat,'b');
end
linkaxes(sh,'xy');
