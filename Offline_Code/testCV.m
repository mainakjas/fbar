function [Yhat] = testCV(Xtest,model,opt)
%% Normalize test features
if isfield(model,'ftrStats')
    for f = 1:size(Xtest,1)
        selftr = model.ftrStats.selected(f);
        Xtest(f,:) = bsxfun(@minus,Xtest(f,:),model.ftrStats.mean(selftr));
        Xtest(f,:) = bsxfun(@rdivide,Xtest(f,:),model.ftrStats.std(selftr));
    end
end

%% Classify Test Data
if strcmp(opt.classify.classifier,'mlp')
    [er,bad,Yhat] = nntest(model,Xtest', targtovec(double(testlabels)));
elseif strcmp(opt.classify.classifier,'svm')
    nData = size(Xtest,2);
    nDivs = floor(nData/1000);
    remainder = rem(nData,1000);
    Yhat = zeros(1,nData);
    for i = 1:nDivs
        inds = (1000*(i-1)+1):1000*i;
        Yhat(inds) = svmclassify(model,Xtest(:,inds)');
    end
    if remainder > 0
        inds = (1000*nDivs+1):nData;
        Yhat(inds) = svmclassify(model,Xtest(:,inds)');
    end
    Yhat(isnan(Yhat)) = 1;
%     Yhat = -(Yhat-1);
elseif strcmp(opt.classify.classifier,'lda')
    Yhat = (model.const + Xtest'*model.linear) > 0;
elseif strcmp(opt.classify.classifier,'qda')
    %         Yhat = (model.const + testftrs_norm'*model.linear + ...
    %             testftrs_norm'*model.quadratic*testftrs_norm) > 0;
    Yhat = zeros(nWins,1);
    linterm = model.const + Xtest'*model.linear;
    for t = 1:nWins
        Yhat(t) = (linterm(t)+Xtest(:,t)'*model.quadratic*Xtest(:,t)) > 0;
    end
end