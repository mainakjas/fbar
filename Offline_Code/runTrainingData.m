%%%%%%%%%%%%%%%%%%%%%
% Notes:
%%%%%%%%%%%%%%%%%%%%

% All possible training sets: artifacts1-12,artifacts_highalpha1-8
DATASETS = {'artifacts1','artifacts2','artifacts3','artifacts_highalpha1','artifacts_highalpha3','artifacts_highalpha4',...
    'artifacts_highalpha5','artifacts_highalpha7','artifacts_highalpha8'};
% DATASETS = {'artifacts_highalpha1','artifacts_highalpha3','artifacts_highalpha4'};

% Data options
opt.data.chan = 1:4;
opt.data.realtime = 1;
% for i = 1:22 %1:22
%     opt.data.testset{i} = ['Kiret',num2str(i),'_IXDATA'];
% end
% opt.data.testset{1} = 'Kiret6_IXDATA';
opt.data.testset{1} = 'artifacts3';

% Plot options
opt.plot.raw = 0;
opt.plot.windows = 0;
opt.plot.features = 0;
opt.plot.selected_features = 0;
opt.plot.testVapp = 0;
opt.plot.testVlabels = 1;
opt.plot.fft = 1;
opt.plot.comparison = 1;
opt.plot.barfft = 0;
opt.plot.TestAllModels = 0;
opt.plot.adjustlabels = 0;
opt.plot.timelock = 0;
opt.plot.csp = 0;
opt.plot.fft = 0;

% Window Options
opt.window.method = 'sliding';
opt.window.winlen = 1;
opt.window.step = 0.1;

opt.filter = [2 40];

% Feature Options
opt.features.K = [25]; 
opt.features.type = 'sqiAll'; %'sqiEogNoRaw','sqiEogWithTheta','sqiEmg';
opt.features.select = 'mrmr_q'; %'none','mrmr_q','mrmr_d'
opt.features.cspcomps = 4;
opt.features.cspband = [1,21];
opt.features.cspstep = 2;
opt.features.cspwidth = 4;
opt.features.fft = 8:2:30;

% Classification Options
opt.classify.labels = 'manual'; % 'manual,'faster','faster+'
opt.classify.ntrain = 4; %'all'
opt.classify.class = 1; % 1 = ALL, 2 = EOG, 3 = EMG
opt.classify.combinechans = 1; % 1 = combine all chans, 2 = combine 1 and 4 or 2 and 3 depending on opt.data.chan(1)
opt.classify.classifier = 'svm'; % 'svm','mlp','lda','qda'
opt.classify.crossval = 1; % crossval over training data (1) or build full model (0)
opt.classify.labelcutoff = 0.2; %0:0.1:0.5;
opt.classify.removelabels = 0;
opt.classify.svm.kernel = 'rbf'; % 'linear','quadratic','polynomial','rbf','mlp','@kfun'
opt.classify.da.prior = 0;
opt.classify.mlp.hidden = 100;
opt.classify.mlp.learningrate = 0.001;
opt.classify.mlp.numepochs  = 25;

% Set defaults for unset options
opt = getOpts(opt);

%% Build Feature Set
% nD = length(DATASETS);save([datestr(clock),'_newResults'],'model')save([datestr(clock),'_newResults'],'model')
% if opt.classify.combinechans == 1
%     FTRS = cell(4,nD);
%     LABELS = cell(4,nD);    
%     FFTS = cell(4,nD);
%     for ch = 1:4
%         for ds = 1:nD
%             [FTRS{ch,ds},LABELS{ch,ds},FFTS{ch,ds}] = extractFTRS(DATASETS{ds},ch,opt);
%         end
%     end
% else
%     FTRS = cell(1,nD);
%     LABELS = cell(1,nD);
%     FFTS = cell(1,nD);
%     for ds = 1:nD
%         [FTRS{:,ds},LABELS{:,ds},FFTS{:,ds}] = extractFTRS(DATASETS{ds},opt.data.chan,opt);
%     end
% end

%% Build Model
if opt.classify.crossval
    Model = doCV(DATASETS,opt);
else
    Model = TrainModel(DATASETS,opt);
end

%% Test Model
if opt.classify.combinechans == 1
    channels = 1:4;
elseif opt.classify.combinechans == 2
    if opt.data.chan(1)==1 || opt.data.chan(1)==4
        channels = [1,4];
    elseif opt.data.chan(1)==2 || opt.data.chan(1)==3
        channels = 2:3;
    end
end
% opt.classify.crossval = 1;
% if ~opt.classify.crossval
%%
channels = 1:4;
for ds = 1:length(opt.data.testset)
    for ch = channels
        opt.data.chan = ch;
        if opt.classify.combinechans == 1
            Result{channels(ch),ds} = PlotModelResults2(Model,opt);
        elseif opt.classify.combinechans == 2
            if ch==1||ch==4
                Result{1,ds} = PlotModelResults2(Model{1},opt);
            elseif ch==2||ch==3
                Result{2,ds} = PlotModelResults2(Model{2},opt);
            end
        else
            Result{channels(ch),ds} = PlotModelResults2(Model{ch,ds},opt);
        end
    end
end
% end
% opt.classify.crossval = 0;



% save EOGmodels



