function [R] = runTest(file,chan)
load EOGmodels3
load(file)
opt.data.chan = chan;
opt.data.realtime = 1; %simulate real-time processing

if exist('IXDATA','var')
    opt.data.testset = file;
elseif exist('EEG','var')
    eeg = concatenateEEG(EEG);
    IXDATA = convert2IXDATA(eeg);
    save testdata IXDATA
    opt.data.testset = 'temptestdata';
end

% Plot options
opt.plot.raw = 0;
opt.plot.windows = 0;
opt.plot.features = 0;
opt.plot.selected_features = 0;
opt.plot.testVapp = 0;
opt.plot.testVlabels = 1;
opt.plot.barfft = 0;
opt.plot.TestAllModels = 0;
opt.plot.adjustlabels = 0;

% Window Options
opt.window.method = 'sliding';
opt.window.winlen = 1;
opt.window.step = 0.1;

% Feature Options
% opt.features.K = 1:28; %1:28
opt.features.type = 'sqiEog';
opt.features.cspcomps = 4;
opt.features.cspband = [1,21];
opt.features.cspstep = 2;
opt.features.cspwidth = 4;
opt.features.fft = 8:2:30;
opt.features.select = 'mrmr_q'; %'mrmr_q'

% Classification Options
opt.classify.class = 2; % 2 = EOG, 3 = EMG
opt.classify.classifier = 'svm'; % 'svm','mlp','lda','qda'
opt.classify.crossval = 1; % crossval over training data (1) or build full model (0)
opt.classify.labelcutoff = 0.3; %0:0.1:0.5;
opt.classify.removelabels = 0;
opt.classify.svm.kernel = 'rbf'; % 'linear','quadratic','polynomial','rbf','mlp','@kfun'

% Run Model
model = EOGmodel{opt.data.chan};
R = TestModel(model,opt);

% delete temporary file
if exist('EEG','var')
    delete temptestdata.mat
end