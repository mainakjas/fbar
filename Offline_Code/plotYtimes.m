function plotYtimes(dataset,Ytimes)

for ch = 1:4
    %% Load dataset
%     if opt.classify.crossval
        [IXDATA,Y] = feval([dataset,'_ManualLabels'],ch);
        if exist([dataset,'.mat'],'file')
            load(dataset);
        end
%     else
%         load(dataset)
%         if ~exist('IXDATA','var')&&exist('EEG','var')
%             eeg = concatenateEEG(EEG);
%             IXDATA = convert2IXDATA(eeg);
%             save(dataset,'IXDATA','-append')
%         elseif ~exist('IXDATA','var')&&exist('muse_eeg_raw','var')
%             % Old version of Muse data
%             IXDATA.raw.eeg.times = muse_eeg_raw(:,1); %#ok<NODEF>
%             IXDATA.raw.eeg.data = muse_eeg_raw(:,2:5);
%             save(dataset,'IXDATA','-append')
%         end
%     end
    
    %% Prepare Data
    X = IXDATA.raw.eeg.data(1:end,ch);
    [TS,FS] = fixTS(IXDATA.raw.eeg.times(1:end));
    Ytrue = Y(2,:) + Y(3,:) + Y(4,:);
       
%     if opt.plot.testVlabels && opt.classify.crossval
        %% Output of Current Model versus Labeled Data
        h2 = figure;
        
        % Plot Classifier output for MLP and APP version
        sh2(1) = subplot(2,1,1);
        goodraw = X; goodraw(Ytimes(ch,:)==1) = nan;
        badraw = X; badraw(Ytimes(ch,:)==0) = nan;
        plot(TS-TS(1),goodraw,'b');
        hold on;
        plot(TS-TS(1),badraw,'r')
        title('Model Classification Output')
        legend('Clean','Artifact')
        
        sh2(2) = subplot(2,1,2);
        goodlabels = X; goodlabels(Ytrue==1) = nan;
        badlabels = X; badlabels(Ytrue==0) = nan;
        plot(TS-TS(1),goodlabels,'b');
        hold on;
        plot(TS-TS(1),badlabels,'r')
        title('True Labels')
        
%         % Plot FFTs
%         if length(size(Xfft)) == 2
%             Xfft_model = log10(mean(Xfft(:,Yhat==0),2));
%             Xfft_true = log10(mean(Xfft(:,labels==0),2));
%             Xfft_mean = log10(mean(Xfft,2));
%         elseif length(size(Xfft)) == 3
%             Xfft_model = log10(mean(Xfft(:,:,Yhat==0),3));
%             Xfft_true = log10(mean(Xfft(:,:,labels==0),3));
%             Xfft_mean = log10(mean(Xfft,3));
%         end
%         Fr = linspace(0,FS/2,size(Xfft,1));
%         
%         sh2(3) = subplot(4,1,3:4);
%         plot(Fr,Xfft_mean(:,1:length(chan)),'k','LineWidth',2)
%         hold on
%         plot(Fr,Xfft_model(:,1:length(chan)),'b','LineWidth',2)
%         hold on
%         plot(Fr,Xfft_true(:,1:length(chan)),'g','LineWidth',2)
%         legend('Total','Model','True')
%         title('FFTs')
%         linkaxes(sh2(1:2),'xy')
%         
%         Result.fft_corr = corr(Xfft_model,Xfft_true);
%         fprintf('FFT Correlation: %g\n',Result.fft_corr);
        
%     end
    
end
end