function [LABELS,metrics] = useFaster(mode,DATASETS)
% DATASETS = {'artifacts1','artifacts2','artifacts3','artifacts_highalpha1','artifacts_highalpha3','artifacts_highalpha4',...
%     'artifacts_highalpha5','artifacts_highalpha7','artifacts_highalpha8'};

nDS = length(DATASETS);
LABELS = cell(nDS,1);
metrics.acc = zeros(4,nDS);
metrics.auc = zeros(4,nDS);
metrics.fpr = zeros(4,nDS);
metrics.fnr = zeros(4,nDS);

for ds = 1:nDS
    %% Load Dataset
    dataset = DATASETS{ds};
    flag = exist([dataset,'_ManualLabels'],'file') == 2;
    
    % Either get raw data or data with labels
    if flag
        Y = cell(4,1);
        [IXDATA,Y{1}] = feval([dataset,'_ManualLabels'],1);
        for ch = 2:4
            [~,Y{ch}] = feval([dataset,'_ManualLabels'],ch);
        end
    else
        load(dataset,'IXDATA');
    end
    
    %% Get EEG data
    nSamps_orig = length(IXDATA.raw.eeg.data);
    DATA = zeros(nSamps_orig,4);
    for ch = 1:4
        DATA(:,ch) = IXDATA.raw.eeg.data(:,ch);
    end
    nSamps = floor(nSamps_orig/220)*220;
    DATA = DATA(1:nSamps,:)';
    
    %% Use EEGLAB to reject artifacts using FASTER algorithm and all other
    % algorithms
    EEG = pop_importdata('data',DATA,'setname','artifacts1','srate',220,'pnts',220,'xmin',-0.9);
    EEG = pop_rmbase(EEG,[]);
    
    rmidx = zeros(4,EEG.trials);
    for ch = 1:4
        [~,idxtemp] = pop_autorej(EEG,'electrodes',ch,'threshold',100,'startprob',3,'nogui','on');
        rmidx(ch,idxtemp) = 1;
    end
    
    if strcmpi(mode,'faster+')
        EEG_prob = pop_jointprob(EEG,1,1:4,2,2,1,0,0);
        EEG_kurt = pop_rejkurt(EEG,1,1:4,2,2,1,0,0);
        rmepochs_kurt = EEG_kurt.reject.rejkurtE;
        EEG_thresh = pop_eegthresh(EEG,1,1:4,[-30,-20,-20,-30],[30,20,20,30],0,1,1,0);
        rmepochs_thresh = EEG_thresh.reject.rejthreshE;
        rmidx = (rmidx + rmepochs_kurt + rmepochs_thresh) > 0;
    end
    
    if flag
        %% Compare artifact rejection to true labels
        Yhat = zeros(4,nSamps_orig);
        Ytrue = zeros(4,nSamps_orig);
        acc = zeros(4,1);
        auc = zeros(4,1);
        fnr = zeros(4,1);
        fpr = zeros(4,1);
        for ch = 1:4
            Ytrue(ch,:) = (Y{ch}(2,:) + Y{ch}(3,:) + Y{ch}(4,:))>0;
            Yhat(ch,1:220) = rmidx(ch,1);
            for i = 2:length(rmidx)-1
                str = 220*(i-1)+1;
                fin = 220*i;
                Yhat(ch,str:fin) = rmidx(ch,i);
            end
            Yhat(ch,fin+1:end) = rmidx(ch,end);
            acc(ch) = mean(Ytrue(ch,:)==Yhat(ch,:));
            if length(unique(Ytrue(ch,:)))>1
                [~,~,~,auc(ch)] = perfcurve(Ytrue(ch,:),Yhat(ch,:),1);
                [~,~,~,per] = confusion(Ytrue(ch,:),Yhat(ch,:));
                fnr(ch) = per(2,1);
                fpr(ch) = per(2,2);
            else
                auc(ch) = nan;
                fnr(ch) = nan;
                fpr(ch) = nan;
            end
        end
        LABELS{ds} = Yhat;
        fprintf('Mean accuracy = %g\n',mean(acc));
        metrics.acc(:,ds) = acc;
        metrics.auc(:,ds) = auc;
        metrics.fpr(:,ds) = fpr;
        metrics.fnr(:,ds) = fnr;
    else
        Yhat = zeros(4,nSamps_orig);
        for ch = 1:4
            Yhat(ch,1:220) = rmidx(ch,1);
            for i = 2:length(rmidx)-1
                str = 220*(i-1)+1;
                fin = 220*i;
                Yhat(ch,str:fin) = rmidx(ch,i);
            end
            Yhat(ch,fin+1:end) = rmidx(ch,end);
        end
        LABELS{ds} = Yhat;
    end
end

%% Plot Results
% plotYtimes(dataset,Yhat);



