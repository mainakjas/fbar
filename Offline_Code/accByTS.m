function acc = accByTS(testset,Yhat,opt)
% Gives the classification accuracy in terms of the time-series
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
nChan = length(opt.data.chan);

nWins = zeros(nChan,1);
if (nChan>1)&&(opt.classify.combinechans>0)
    chans = opt.data.chan;
    idx = 1;
    for ch = 1:nChan
        opt.data.chan = chans(ch);
        [~,LABS,~] = getTrainingData({testset},opt);
        nWins(ch) = length(LABS{1});
        yhat{ch} = Yhat(idx:idx+nWins(ch)-1);
        idx = idx+nWins(ch);
    end
else
    nWins = length(Yhat);
    yhat{1} = Yhat;
end

% Get time-series labels
chanAcc = zeros(nChan,1);
for ch = 1:nChan
    % Get true time-series labels
    [IXDATA,Y] = feval([testset,'_ManualLabels'],chans(ch));
    Ytrue_ts = Y(opt.classify.class,:);
    
    % Get sampling rate and window params for window to time-series conversion
    [~,FS] = fixTS(IXDATA.raw.eeg.times(1:end));
    winlen = ceil(FS*opt.window.winlen);
    step = floor(FS*opt.window.step);
    TS = zeros(nWins(ch),1);
    for i = 1:nWins(ch)
        ind = i*step;
        TS(i) = floor(ind+winlen/2);
    end
    
    % Convert model labels to time-series
    Yhat_ts = zeros(1,size(Y,2));
    Yhat_ts(1:TS(1)) = yhat{ch}(1);
    for win = 2:nWins(ch)-1
        Yhat_ts(TS(win-1):TS(win)) = yhat{ch}(win);
        if (yhat{ch}(win)~=yhat{ch}(win-1))&&(yhat{ch}(win)~=yhat{ch}(win+1))
            Yhat_ts(TS(win-1):TS(win)) = yhat{ch}(win-1);
        end
    end
    Yhat_ts(TS(win):end) = yhat{ch}(nWins(ch));
    
    % Ignore data from other classes
    keepidx = Y(1,:)|Y(opt.classify.class,:); 
    Ytrue_ts = Ytrue_ts(keepidx);
    Yhat_ts = Yhat_ts(keepidx);
    
    % Compute time-series accuracy
    chanAcc(ch) = mean(Ytrue_ts==Yhat_ts)*100;
end

acc = mean(chanAcc);