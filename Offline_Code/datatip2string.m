

clear x
for i = length(cursor_info):-1:1; x(length(cursor_info)-i+1) = cursor_info(i).Position(1); end
x = sort(x);

if mod(length(x),2)==1 % is odd
    error('KD: not an even number of datatips')
end

string = [];
for i = 1:length(x)/2
    string = [string,num2str(x(i*2-1)),':',num2str(x(i*2)),','];
end
string