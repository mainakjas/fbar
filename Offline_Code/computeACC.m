function acc = computeACC(R,ch)
rm = R.Ytrue{ch}(7,:);
acc = mean(R.Ytimes{ch}(~rm)~=R.Ytrue{ch}(1,~rm)');