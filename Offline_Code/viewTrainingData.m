function viewTrainingData(dataset)

sh = zeros(4,1);
figure;
for chan = 1:4
    sh(chan) = subplot(4,1,chan);
    % Load and plot raw data
    [IXDATA,Y] = feval([dataset,'_ManualLabels'],chan);
    dat1 = IXDATA.raw.eeg.data(:,chan);
    plot(dat1,'b'); hold on
    dat2 = dat1; dat3 = dat1; dat4 = dat1;
    % Add classified labels if available
    if exist([dataset,'_Ytimes.mat'],'file') == 2
%         Y = addYtimes(dataset,Y,chan);
    end
    % Separate data by labels
    dat1(Y(1,:)==1) = nan;
    dat2(Y(2,:)==0) = nan;
    dat3(Y(3,:)==0) = nan;
    dat4(Y(4,:)==0) = nan;
    % Plot data with color-coded labels
    plot(dat1,'b');
    plot(dat2,'r')
    plot(dat3,'m')
    plot(dat4,'k')
end
linkaxes(sh,'xy');
legend('Not Clean','Clean EEG','LF artifacts','EMG hard','EMG light');

end

function [Y] = addYtimes(dataset,Y,chan)
load([dataset,'_Ytimes.mat']);
if exist(['Ytimes_ch',num2str(chan),'_lowfreq'],'var')
    Y(2,:) = Y(2,:)'|eval(['Ytimes_ch',num2str(chan),'_lowfreq']);
end
if exist(['Ytimes_ch',num2str(chan),'_emg_hard'],'var')
    Y(3,:) = Y(3,:)'|eval(['Ytimes_ch',num2str(chan),'_emg_hard']);
end
if exist(['Ytimes_ch',num2str(chan),'_emg_light'],'var')
    Y(4,:) = Y(4,:)'|eval(['Ytimes_ch',num2str(chan),'_emg_hard']);
end
end