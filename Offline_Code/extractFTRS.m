function [Xftrs,labels,Xfft] = extractFTRS(filename,chan,opt)

tic;
fprintf('Computing ftrs for chan %g\n',chan);
load(filename)

%% Get windowing data and parameters
% Specific options for certain feature types
if strcmp(opt.features.type,'fbcsp')
    chan = opt.data.chan;
    % if each channel has a unique Y, use sum(Y)>0
end

% Get raw data
[IXDATA,Y] = feval([filename,'_ManualLabels'],chan);
[TS,FS] = fixTS(IXDATA.raw.eeg.times); % Need to fix timestamps from Muse data
if opt.classify.class == 1
    X = IXDATA.raw.eeg.data(:,chan);
    Y = ~Y(1,:);
else
    keep = (Y(1,:)==1)|(Y(opt.classify.class,:)==1);
    X = IXDATA.raw.eeg.data(keep,chan);
    Y = Y(:,keep);
end

% Windowing parameters
winlen = ceil(FS*opt.window.winlen);
step = floor(FS*opt.window.step);
nWins = length(1:step:(length(X)-winlen-step));

%% Initialize features matrices
if strcmpi(opt.features.type,'sqi+')
    Xftrs = zeros(40,nWins);
elseif strcmpi(opt.features.type,'sqiEog')
    Xftrs = zeros(28,nWins);
elseif strcmpi(opt.features.type,'sqiEogWithTheta')
    Xftrs = zeros(30,nWins);
elseif strcmpi(opt.features.type,'sqiEogNoRaw')
    Xftrs = zeros(24,nWins);
elseif strcmpi(opt.features.type,'sqiEogNoAlpha')
    Xftrs = zeros(22,nWins);
elseif strcmpi(opt.features.type,'sqiEmg')
    Xftrs = zeros(18,nWins);
elseif strcmpi(opt.features.type,'sqiAll')
    Xftrs = zeros(54,nWins);
end
labels = zeros(1,nWins);
if mod(FS/2,1) == 0
    Xfft = squeeze(zeros(floor(FS/2),length(chan),nWins));
else
    Xfft = squeeze(zeros(floor(FS/2)+1,length(chan),nWins));
end

%% Extract features and FFTs
TSplot = zeros(1,nWins);
TStest2 = zeros(1,nWins);
for i = 1:nWins
    % Save window indices
    ind = i*step;
    TSplot(i) = floor(ind+winlen/2);
    TStest2(i) = TS(floor(ind+winlen/2))-TS(1);
    
    % Extract Features
    if strcmp(opt.features.type,'handcode')
        Xftrs(:,i) = buildftrs(X(ind:ind+winlen-1,:),newlab(i),chan,FS,0);
    elseif strcmp(opt.features.type,'sqi')
        Xftrs(:,i) = sqi_ftrs(X(ind:ind+winlen-1),FS)';
    elseif strcmp(opt.features.type,'sqi+')
        Xftrs(:,i) = sqiplus_ftrs(X(ind:ind+winlen-1),FS)';
    elseif strcmp(opt.features.type,'ar')
        Xftrs(:,i) = getARfeatures(X(ind:ind+winlen-1)',4,1);
    elseif strcmp(opt.features.type,'raw')
        Xftrs(:,i) = (X(ind:ind+winlen-1,chan)-mean(X(ind:ind+winlen-1,chan)))';
    elseif strcmp(opt.features.type,'sqi+-ar')
        Xftrs(:,i) = [sqiplus_ftrs(X(ind:ind+winlen-1),FS)',getARfeatures(X(ind:ind+winlen-1,chan)',4,1)];
    elseif strcmp(opt.features.type,'sqinew')
        Xftrs(:,i) = sqinew_ftrs(X(ind:ind+winlen-1),FS)';
    elseif strcmp(opt.features.type,'sqiEog')
        Xftrs(:,i) = sqiEog_ftrs(X(ind:ind+winlen-1),FS)';
    elseif strcmp(opt.features.type,'sqiEogNoRaw')
        Xftrs(:,i) = sqiEogNoRaw_ftrs(X(ind:ind+winlen-1),FS)';
    elseif strcmp(opt.features.type,'sqiEogWithTheta')
        Xftrs(:,i) = sqiEogWithTheta_ftrs(X(ind:ind+winlen-1),FS)';
    elseif strcmp(opt.features.type,'sqiEogNoAlpha')
        Xftrs(:,i) = sqiEogNoAlpha_ftrs(X(ind:ind+winlen-1),FS)';
    elseif strcmp(opt.features.type,'sqiEmg')
        Xftrs(:,i) = sqiEmg_ftrs(X(ind:ind+winlen-1),FS)';
    elseif strcmp(opt.features.type,'sqiAll')
        Xftrs(:,i) = sqiAll_ftrs(X(ind:ind+winlen-1),FS)';
    elseif strcmp(opt.features.type,'sqitest')
        Xftrs(:,i) = sqitest(X(ind:ind+winlen-1),FS)';
    elseif strcmp(opt.features.type,'fbcsp')
        Xftrs(:,:,i) = bsxfun(@minus,X(ind:ind+winlen-1,:),mean(X(ind:ind+winlen-1,:)))';
    end
    
    % Get Labels
    labels(i) = mean(Y(opt.classify.class,ind:ind+winlen-1)) > opt.classify.labelcutoff;
    
    % Compute FFTs for FFT plots
    H = hamming(winlen);
    %         HP = sum(hamming(winlen)).^2;
    xtemp = bsxfun(@minus,X(ind:ind+winlen-1),mean(X(ind:ind+winlen-1)));
    psd = (fft(bsxfun(@times,H,xtemp)))/winlen;%(winlen*HP);
    if floor(FS/2) == size(Xfft,1)
        if ndims(Xfft) == 2 %#ok<*ISMAT>
            Xfft(:,i) = abs(psd(1:floor(FS/2))).^2;
        elseif ndims(Xfft) == 3
            Xfft(:,:,i) = abs(psd(1:floor(FS/2),:)).^2;
        end
    else
        if ndims(Xfft) == 2
            Xfft(:,i) = abs(psd(1:floor(FS/2)+1,:)).^2;
        elseif ndims(Xfft) == 3
            Xfft(:,:,i) = abs(psd(1:floor(FS/2)+1,:)).^2;
        end
    end
end
Xftrs(isnan(Xftrs)) = 100;

%% Compute FBCSP features if called for
if strcmp(opt.features.type,'fbcsp')
    opt.features.cspcomps = opt.features.cspcomps/2;
    opt.filter.type = 'butter';
    opt.filter.order = 4;
    opt.filter.sr = FS;
    opt.mode = 'ourcat';
    
    csp_trainind = logical(ones(nWins,1));
    if opt.data.numClasses == 2
        [nCSP,nChans,nBands] = size(opt.filter.cspfilters);
        Xcsp = zeros(nCSP*nBands,size(Xftrs,2),nWins);
        Xcspftrs = zeros(nCSP*nBands,nWins);
        for band = 1:size(opt.filter.cspfilters,3)
            bandInds = (nCSP*(band-1)+1):band*nCSP;
            CSPfilter = squeeze(opt.filter.cspfilters(:,:,band));
            for i = 1:nWins
                Xcsp(bandInds,:,i) = CSPfilter*Xftrs(:,:,i);
                xvars = var(Xcsp(bandInds,:,i),[],2);
                Xcspftrs(bandInds,i) = log10(xvars/sum(xvars));
            end
        end
    elseif opt.data.numClasses >= 2
        % Placeholder for multiclass FBCSP
    end
    Xftrs = Xcspftrs;
end

% Save the feature set for later use
eval(sprintf('ftrs_%s{chan}=Xftrs;',opt.features.type));
save(filename,['ftrs_',opt.features.type],'-append');
eval(sprintf('fft_%s{chan}=Xfft;',opt.features.type));
save(filename,['fft_',opt.features.type],'-append');
eval(sprintf('labels_%s{chan}=labels;',opt.features.type));
save(filename,['labels_',opt.features.type],'-append');
save(filename,'TSplot','-append');
fprintf('   Took %gs\n',toc);


% nDS = length(DATASETS);
% nCH = max(opt.data.chan);
% FTRS = cell(nC,nD);
% Y = cell(nC,nD);

% for chan = opt.data.chan
%     for ds = 1:nD
%         % Check if the set has a feature file to load, or create one
%         fprintf('Feature Extraction for file %d... ',ds)
%         setname = [DATASETS{ds},'_ManualLabels'];
%         ftrfilename = [DATASETS{ds},'_',opt.features.type,'_traindata.mat'];
%         labelfile = which(setname);
%         ftrfile = which(ftrfilename);
%         if ~isempty(ftrfile)
%             %% If feature file exists, just load the feature matrix
%             fprintf('Loading Feature Matrix\n')
%             load(ftrfile,'features','labels')
%             FTRS{chan,ds} = features;
%             Y{chan,ds} = labels;
%         elseif ~isempty(labelfile)
%             %% If no feature file, build feature matrix and create feature file
%             fprintf('Need to create a feature matrix... '); tic;
%             [IXDATA,Ytimes] = feval(setname,chan);
%             [TS,FS] = fixTS(IXDATA.raw.eeg.times); % Need to fix timestamps
%             winlen = ceil(opt.window.winlen*FS);
%             step = floor(opt.window.step*FS);
%             
%             % Extract features per set of class definitions
%             class = opt.classify.class;
%             %             for class = opt.classify.class
%             % Initialize Data and Class Labels
%             %                 clear Xclass Yclass nWins Xftrs ftrs labels
%             if class == 1
%                 Xclass = IXDATA.raw.eeg.data(:,chan);
%                 Yclass = Ytimes;
%             else
%                 Xclass = IXDATA.raw.eeg.data(Ytimes(1,:)==1|Ytimes(class,:)==1,chan);
%                 Yclass = Ytimes(class,Ytimes(1,:)==1|Ytimes(class,:)==1);
%             end
%             nWins = length(1:step:(length(Xclass)-winlen-step));
%             
%             % Initialize window matrix, feature matrix, and label vector
%             Xwins = zeros(winlen,length(chan),nWins);
%             if strcmp(opt.features.type,'sqi+')
%                 features = zeros(40,nWins);
%             elseif strcmp(opt.features.type,'sqiEog')
%                 features = zeros(28,nWins);
%             elseif strcmp(opt.features.type,'sqiEmg')
%                 features = zeros(22,nWins);
%             elseif strcmp(opt.features.type,'sqiAll')
%                 features = zeros(46,nWins);
%             elseif strcmp(opt.features.type,'sqitest')
%                 features = zeros(28,nWins);
%             end
%             labels = zeros(1,nWins);
%         
%             % Extract chosen features per epoch
%             for i = 1:nWins
%                 ind = i*step;
%                 Xwins(:,:,i) = Xclass(ind:ind+winlen-1,:);
%                 if strcmp(opt.features.type,'handcode')
%                     features(:,i) = buildftrs(Xclass(ind:ind+winlen-1,:),newlab(i),chan,FS,0);
%                 elseif strcmp(opt.features.type,'sqi')
%                     features(:,i) = sqi_ftrs(Xclass(ind:ind+winlen-1),FS)';
%                 elseif strcmp(opt.features.type,'sqi+')
%                     features(:,i) = sqiplus_ftrs(Xclass(ind:ind+winlen-1),FS)';
%                 elseif strcmp(opt.features.type,'ar')
%                     features(:,i) = getARfeatures(Xclass(ind:ind+winlen-1)',4,1);
%                 elseif strcmp(opt.features.type,'raw')
%                     features(:,i) = (Xclass(ind:ind+winlen-1,chan)-mean(Xclass(ind:ind+winlen-1,chan)))';
%                 elseif strcmp(opt.features.type,'sqi+-ar')
%                     features(:,i) = [sqiplus_ftrs(Xclass(ind:ind+winlen-1),FS)',getARfeatures(Xclass(ind:ind+winlen-1,chan)',4,1)];
%                 elseif strcmp(opt.features.type,'sqinew')
%                     features(:,i) = sqinew_ftrs(Xclass(ind:ind+winlen-1),FS)';
%                 elseif strcmp(opt.features.type,'sqiEog')
%                     features(:,i) = sqiEog_ftrs(Xclass(ind:ind+winlen-1),FS)';
%                 elseif strcmp(opt.features.type,'sqiEmg')
%                     features(:,i) = sqiEmg_ftrs(Xclass(ind:ind+winlen-1),FS)';
%                 elseif strcmp(opt.features.type,'sqiAll')
%                     features(:,i) = sqiAll_ftrs(Xclass(ind:ind+winlen-1),FS)';
%                 elseif strcmp(opt.features.type,'sqitest')
%                     features(:,i) = sqitest(Xclass(ind:ind+winlen-1),FS)';
%                 elseif strcmp(opt.features.type,'fbcsp')
%                     features(:,:,i) = bsxfun(@minus,Xclass(ind:ind+winlen-1,:),mean(Xclass(ind:ind+winlen-1,:)))';
%                 end
%                 labels(i) = mean((Yclass(ind:ind+winlen-1)));
%             end
%             % Remove nan samples
%             [~,nancols] = find(isnan(features));
%             nancols = unique(nancols);
%             nWins = nWins - length((nancols));
%             labels(nancols) = [];
%             if ismatrix(features)
%                 features(:,nancols) = [];
%             elseif ndims(features)==3
%                 features(:,:,nancols) = [];
%             end
%             
%             % Label binarization and removing ambiguous windows
%             %                 if opt.classify.labelcutoff > 0
%             %                     rmwindows = (lables>0)&labels<=opt.classify.labelcutoff;
%             %                     nWins = nWins - sum(rmwindows);
%             %                     labels(rmwindows) = [];
%             %                     if lndims(Xftrs)==2
%             %                         Xftrs(:,rmwindows) = [];
%             %                     elseif ndims(Xftrs)==3
%             %                         Xftrs(:,:,rmwindows) = [];
%             %                     end
%             %                 end
%             labels = labels > opt.classify.labelcutoff;
%             %                 opt.data.numClasses = length(unique(labels));
%             
%             % Compute FBCSP features outside of loop
%             if strcmp(opt.features.type,'fbcsp')
%                 opt.features.cspcomps = opt.features.cspcomps/2;
%                 opt.filter.type = 'butter';
%                 opt.filter.order = 4;
%                 opt.filter.sr = FS;
%                 opt.mode = 'ourcat';
%                 
%                 csp_trainind = true(nWins,1);
%                 %                     if opt.data.numClasses == 2
%                 [Xcsp,features,Xcsp2,CSP_filters] = getSWDCSPfeatures(features,double(labels)+1,csp_trainind,opt);
%                 %                     elseif opt.data.numClasses >= 2
%                 %                 for multi = 1:3
%                 %                     Ynew = Y;
%                 %                     if multi == 1
%                 %                         Ynew(Ynew~=1) = 2;
%                 %                     elseif multi == 2
%                 %                         Ynew(Ynew~=2) = 1;
%                 %                     elseif multi == 3
%                 %                         Ynew(Ynew~=3) = 2;
%                 %                         Ynew = Ynew - 1;
%                 %                     end
%                 %                     [Xcsp,cspftrs_multi{multi},Xcsp2,Psel] = getSWDCSPfeatures(X,Ynew,trainind,opt);
%                 %                 end
%                 %                 cspftrs = [cspftrs_multi{1};cspftrs_multi{2};cspftrs_multi{3}];
%                 %                     end
%                 opt.features.cspcomps = opt.features.cspcomps*2;
%                 opt.filter.cspfilters = CSP_filters;
%             end
%             
%             %% Populate total matrices and save feature file
%             FTRS{chan,ds} = features;
%             Y{chan,ds} = labels;
%             savename = [labelfile(1:strfind(labelfile,setname)-1),ftrfilename];
%             save(savename,'features','labels')
%             fprintf('Took %d seconds\n',round(toc));
%             %             end
%         end 
%     end
% end
