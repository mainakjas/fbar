function Yhat = getNewLabels(testset,chan)

opt.data.chan = chan;
opt.data.realtime = 1;
opt.data.testset = testset;

% Plot options
opt.plot.raw = 0;
opt.plot.windows = 0;
opt.plot.features = 0;
opt.plot.selected_features = 0;
opt.plot.testVapp = 1;
opt.plot.testVlabels = 0;
opt.plot.barfft = 0;
opt.plot.TestAllModels = 0;
opt.plot.adjustlabels = 0;

% Window Options
opt.window.method = 'sliding';
opt.window.winlen = 1;
opt.window.step = 0.1;

% Feature Options
opt.features.K = 15; %1:28
opt.features.type = 'sqiEog';
opt.features.cspcomps = 4;
opt.features.cspband = [1,21];
opt.features.cspstep = 2;
opt.features.cspwidth = 4;
opt.features.fft = 8:2:30;
opt.features.select = 'mrmr_q'; %'mrmr_q'

% Classification Options
opt.classify.class = 2; % 2 = EOG, 3 = EMG
opt.classify.classifier = 'svm'; % 'svm','mlp','lda','qda'
opt.classify.crossval = 1; % crossval over training data (1) or build full model (0)
opt.classify.labelcutoff = 0.2; %0:0.1:0.5;
opt.classify.removelabels = 0;

opt.classify.da.prior = 0;

opt.classify.svm.kernel = 'rbf'; % 'linear','quadratic','polynomial','rbf','mlp','@kfun'

opt.classify.mlp.hidden = 50;
opt.classify.mlp.learningrate = 0.001;
opt.classify.mlp.numepochs  = 75;

%%
load EOGmodels2
model = EOGmodel{chan};
R = TestModel(model,opt);
Yhat = R.Yhat{chan};