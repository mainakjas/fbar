function model = TrainModel(DATASETS,opt)
%% HELP AND USAGE
% Gives the classification accuracy per cross-validation run for a dataset
% given the options set in opt. Default options will be set for any option
% not explicitly defined in opt.
%
% INPUTS:
%   DATASETS: cell array of strings denoting the filenames for each dataset
%       to be used to train the classification models.
%       Current Labelled Datasets:
%           artifacts1,artifacts2,artifacts3,artifacts_highalpha1,
%           artifacts_highalpha3,artifacts_highalpha4,artifacts_highalpha5,
%           artifacts_highalpha7,artifacts_highalpha8
%   Options:
%       See getOpts.m for options
%
% OUTPUTS:
%   model: a structure array containing a classification model for each k
%       in opt.features.k
%   
%
% Written by Kiret Dhindsa
%   Last updated: 07/18/2016
%% Options
opt = getOpts(opt);
K = opt.features.K;
nK = length(K);

if strcmp(opt.classify.classifier,'svm')
    svm_opt = statset('MaxIter',10^6,'Display','off');
end

if opt.classify.combinechans == 0
    opt.data.chan = opt.data.chan(1);
    if length(opt.data.chan)>1
        warning('\nOnly computing model for chan %d. Use TrainModel.m separately for each channel.\n',opt.data.chan(1));
    end
elseif opt.classify.combinechans == 1
    opt.data.chan = 1:4;
elseif opt.classify.combinechans == 2
    if opt.data.chan(1)==1 || opt.data.chan(1)==4
        opt.data.chan = [1,4];
    elseif opt.data.chan(1)==2 || opt.data.chan(1)==3
        opt.data.chan = 2:3;
    end
end

%% Warning and Error Messages for Incorrect Usage
if strcmp(opt.features.type,'fbcsp') && ~isvector(opt.data.chan)
    error('KD: FBCSP requires multiple channels')
end
if strcmp(opt.features.type,'fbcsp') && (length(opt.data.chan) < opt.features.cspcomps)
    error('KD: CSP cannot output more components than the number of input channels')
end

% Set figures to not come to foreground as they are generated
% set(0,'DefaultFigureVisible','off');

%% Get Features
fprintf('Getting training data... ');
tic;
[FTR,LAB,~] = getTrainingData(DATASETS,opt);
ftrs = cell2mat(FTR');
labels = cell2mat(LAB');

% Normalize Features
ftrs_max = max(ftrs,[],2);
ftrs_mean = mean(ftrs,2);
ftrs_std = std(ftrs,[],2);
ftrs_norm = zeros(size(ftrs));
for f = 1:size(ftrs,1)
    ftrs_norm(f,:) = bsxfun(@minus,ftrs(f,:),ftrs_mean(f));
    ftrs_norm(f,:) = bsxfun(@rdivide,ftrs_norm(f,:),ftrs_std(f));
end
FTRS = ftrs_norm;

% Store feature statistics
[nFtrs,nWins] = size(FTRS);
ftrStats.mean = ftrs_mean;
ftrStats.std = ftrs_std;
ftrStats.max = ftrs_max;

fprintf('Took %g seconds.\n',toc);

%% Classification
model(nK).ftr_type = opt.features.type;

% Feature selection
if strcmp(opt.features.select,'mrmr_d')
    selected = mrmr_mid_d(FTRS',labels,max(K));
elseif strcmp(opt.features.select,'mrmr_q')
    selected = mrmr_miq_d(FTRS',labels,max(K));
else
    selected = 1:nFtrs;
    K = nFtrs;
    nK = 1;
    opt.features.K = nFtrs;
end
        
% Loop over selected feature sets     
for k = 1:nK
    fprintf('Building model %g / %g (k = %g)... ',k,nK,K(k));
    tic;
    FTRSsel = FTRS(selected(1:K(k)),:);
    ftrStats.selected = selected(1:K(k));
    
    % Build model
    model(k).ftr_type = opt.features.type;
    if strcmp(opt.classify.classifier,'mlp')
        % Neural Network
        factors = factor(length(labels));
        [~,~,opts.batchsize] = find(factors(factors<=25),1,'last');
        if isempty(opts.batchsize); opts.batchsize = min(factors); end
        opts.numepochs = opt.classify.mlp.numepochs;
        opts.silent = 0;
        opts.plot = 0;
        model(k) = nnsetup([size(FTRSsel,1),opt.classify.mlp.hidden,2]);
        model(k).output               = 'softmax'; %  use softmax output
        model(k).learningRate         = opt.classify.mlp.learningrate;
        model(k).momentum             = 0.9;
        model(k).dropoutFraction      = 0.0;
        model(k).scaling_learningRate = 1;
        % model.sparsityTarget       = 0.0;
        model(k) = nntrain(model(k),FTRSsel',targtovec(double(labels)),opts);
        model(k).ftrStats = ftrStats;
        model(k).opt.mlp_opt = opts;
    elseif strcmp(opt.classify.classifier,'svm')
        % Support Vector Machine
        tempmodel = svmtrain(FTRSsel',labels',...
            'options',svm_opt,'kernel_function',opt.classify.svm.kernel);
        model(k).SupportVectors = tempmodel.SupportVectors;
        model(k).Alpha = tempmodel.Alpha;
        model(k).Bias = tempmodel.Bias;
        model(k).KernelFunction = tempmodel.KernelFunction;
        model(k).KernelFunctionArgs = tempmodel.KernelFunctionArgs;
        model(k).GroupNames = tempmodel.GroupNames;
        model(k).SupportVectorIndices = tempmodel.SupportVectorIndices;
        model(k).ScaleData = tempmodel.ScaleData;
        model(k).FigureHandles = tempmodel.FigureHandles;
        model(k).opt.svm_opt = svm_opt;
    elseif strcmp(opt.classify.classifier,'lda')
        % Linear Discriminant Analysis
        [~,~,~,~,coeff] = classify(zeros(1,size(FTRSsel,1)),FTRSsel',labels','linear');
        model(k).const = coeff(1,2).const;
        model(k).linear = coeff(1,2).linear;
    elseif strcmp(opt.classify.classifier,'qda')
        % Quadratic Discriminant Analysis
        [~,~,~,~,coeff] = classify(zeros(1,size(FTRSsel,1)),FTRSsel',labels','quadratic');
        model(k).const = coeff(1,2).const;
        model(k).linear = coeff(1,2).linear;
        model(k).quadratic = coeff(1,2).quadratic;
    end
    % Add some model information
    model(k).ftrStats = ftrStats;
    model(k).opt = opt;
    model(k).datasets = DATASETS;  
    
    fprintf('Took %g seconds.\n',toc);
end
end
