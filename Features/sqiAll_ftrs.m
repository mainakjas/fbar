function ftrs = sqiAll_ftrs(sig,Fs)

% [N,Nchans] = size(sig);
signorm = bsxfun(@minus,sig,mean(sig));
% signorm = zscore(sig);

%% Raw Signal Features
% base_f1 = 1;
% base_f2 = 48;
% [b,a] = butter(3,[fq2dec(base_f1,Fs),fq2dec(base_f2,Fs)]);
% base_sig = filter(b,a,signorm);
% 
% max_amp = max(abs(base_sig),[],1);
% std_amp = std(base_sig,[],1);
% kurt = kurtosis(base_sig,[],1);
% skew = skewness(base_sig,[],1);
% 
% raw_ftrs = cat(1,max_amp,std_amp,kurt,skew);

%% Frequency band features
% lowfreq Band features
lowfreq_f1 = 0.5;
lowfreq_f2 = 4;
[b,a] = butter(3,[fq2dec(lowfreq_f1,Fs),fq2dec(lowfreq_f2,Fs)]);
lowfreq_sig = filter(b,a,signorm);
lowfreq_pow = calc_fft(signorm,Fs,lowfreq_f1,lowfreq_f2);
    
lowfreq_mean_pow = mean(lowfreq_pow,1);
lowfreq_std_pow = std(lowfreq_pow,[],1);

lowfreq_max_amp = max(abs(lowfreq_sig),[],1);
lowfreq_std_amp = std(lowfreq_sig,[],1);
lowfreq_kurt = kurtosis(lowfreq_sig,[],1);
lowfreq_skew = skewness(lowfreq_sig,[],1);

lowfreq_ftrs = cat(1,lowfreq_mean_pow,lowfreq_std_pow,lowfreq_max_amp,lowfreq_std_amp,lowfreq_kurt,lowfreq_skew);

% Theta Band features
theta_f1 = 4;
theta_f2 = 8;
[b,a] = butter(3,[fq2dec(theta_f1,Fs),fq2dec(theta_f2,Fs)]);
theta_sig = filter(b,a,signorm);
theta_pow = calc_fft(signorm,Fs,theta_f1,theta_f2);
    
theta_mean_pow = mean(theta_pow,1);
theta_std_pow = std(theta_pow,[],1);

theta_max_amp = max(abs(theta_sig),[],1);
theta_std_amp = std(theta_sig,[],1);
theta_kurt = kurtosis(theta_sig,[],1);
theta_skew = skewness(theta_sig,[],1);

theta_ftrs = cat(1,theta_mean_pow,theta_std_pow,theta_max_amp,theta_std_amp,theta_kurt,theta_skew);

% Alpha Band features
alpha_f1 = 8;
alpha_f2 = 13;
[b,a] = butter(3,[fq2dec(alpha_f1,Fs),fq2dec(alpha_f2,Fs)]);
alpha_sig = filter(b,a,signorm);
alpha_pow = calc_fft(signorm,Fs,alpha_f1,alpha_f2);
    
alpha_mean_pow = mean(alpha_pow,1);
alpha_std_pow = std(alpha_pow,[],1);

alpha_max_amp = max(alpha_sig,[],1);
alpha_std_amp = std(alpha_sig,[],1);
alpha_kurt = kurtosis(alpha_sig,[],1);
alpha_skew = skewness(alpha_sig,[],1);

alpha_ftrs = cat(1,alpha_mean_pow,alpha_std_pow,alpha_max_amp,alpha_std_amp,alpha_kurt,alpha_skew);

% Beta1 Band features
beta1_f1 = 13;
beta1_f2 = 18;
[b,a] = butter(3,[fq2dec(beta1_f1,Fs),fq2dec(beta1_f2,Fs)]);
beta1_sig = filter(b,a,signorm);
beta1_pow = calc_fft(signorm,Fs,beta1_f1,beta1_f2);
    
beta1_mean_pow = mean(beta1_pow,1);
beta1_std_pow = std(beta1_pow,[],1);

beta1_max_amp = max(beta1_sig,[],1);
beta1_std_amp = std(beta1_sig,[],1);
beta1_kurt = kurtosis(beta1_sig,[],1);
beta1_skew = skewness(beta1_sig,[],1);

beta1_ftrs = cat(1,beta1_mean_pow,beta1_std_pow,beta1_max_amp,beta1_std_amp,beta1_kurt,beta1_skew);

% Beta2 Band features
beta2_f1 = 18;
beta2_f2 = 25;
[b,a] = butter(3,[fq2dec(beta2_f1,Fs),fq2dec(beta2_f2,Fs)]);
beta2_sig = filter(b,a,signorm);
beta2_pow = calc_fft(signorm,Fs,beta2_f1,beta2_f2);
    
beta2_mean_pow = mean(beta2_pow,1);
beta2_std_pow = std(beta2_pow,[],1);

beta2_max_amp = max(beta2_sig,[],1);
beta2_std_amp = std(beta2_sig,[],1);
beta2_kurt = kurtosis(beta2_sig,[],1);
beta2_skew = skewness(beta2_sig,[],1);

beta2_ftrs = cat(1,beta2_mean_pow,beta2_std_pow,beta2_max_amp,beta2_std_amp,beta2_kurt,beta2_skew);

% Beta3 Band features
beta3_f1 = 25;
beta3_f2 = 30;
[b,a] = butter(3,[fq2dec(beta3_f1,Fs),fq2dec(beta3_f2,Fs)]);
beta3_sig = filter(b,a,signorm);
beta3_pow = calc_fft(signorm,Fs,beta3_f1,beta3_f2);
    
beta3_mean_pow = mean(beta3_pow,1);
beta3_std_pow = std(beta3_pow,[],1);

beta3_max_amp = max(beta3_sig,[],1);
beta3_std_amp = std(beta3_sig,[],1);
beta3_kurt = kurtosis(beta3_sig,[],1);
beta3_skew = skewness(beta3_sig,[],1);

beta3_ftrs = cat(1,beta3_mean_pow,beta3_std_pow,beta3_max_amp,beta3_std_amp,beta3_kurt,beta3_skew);

% Gamma Band features
gamma_f1 = 30;
gamma_f2 = 45;
[b,a] = butter(3,[fq2dec(gamma_f1,Fs),fq2dec(gamma_f2,Fs)]);
gamma_sig = filter(b,a,signorm);
gamma_pow = calc_fft(signorm,Fs,gamma_f1,gamma_f2);
    
gamma_mean_pow = mean(gamma_pow,1);
gamma_std_pow = std(gamma_pow,[],1);

gamma_max_amp = max(gamma_sig,[],1);
gamma_std_amp = std(gamma_sig,[],1);
gamma_kurt = kurtosis(gamma_sig,[],1);
gamma_skew = skewness(gamma_sig,[],1);

gamma_ftrs = cat(1,gamma_mean_pow,gamma_std_pow,gamma_max_amp,gamma_std_amp,gamma_kurt,gamma_skew);

% High Band features
high_f1 = 65;
high_f2 = 80;
[b,a] = butter(3,[fq2dec(high_f1,Fs),fq2dec(high_f2,Fs)]);
high_sig = filter(b,a,signorm);
high_pow = calc_fft(signorm,Fs,high_f1,high_f2);
    
high_mean_pow = mean(high_pow,1);
high_std_pow = std(high_pow,[],1);

high_max_amp = max(high_sig,[],1);
high_std_amp = std(high_sig,[],1);
high_kurt = kurtosis(high_sig,[],1);
high_skew = skewness(high_sig,[],1);

high_ftrs = cat(1,high_mean_pow,high_std_pow,high_max_amp,high_std_amp,high_kurt,high_skew);

% Super High Band features
super_f1 = 80;
super_f2 = 100;
[b,a] = butter(3,[fq2dec(super_f1,Fs),fq2dec(super_f2,Fs)]);
super_sig = filter(b,a,signorm);
super_pow = calc_fft(signorm,Fs,super_f1,super_f2);
    
super_mean_pow = mean(super_pow,1);
super_std_pow = std(super_pow,[],1);

super_max_amp = max(super_sig,[],1);
super_std_amp = std(super_sig,[],1);
super_kurt = kurtosis(super_sig,[],1);
super_skew = skewness(super_sig,[],1);

super_ftrs = cat(1,super_mean_pow,super_std_pow,super_max_amp,super_std_amp,super_kurt,super_skew);


%% Combine all features
ftrs = cat(1,lowfreq_ftrs,theta_ftrs,alpha_ftrs,beta1_ftrs,beta2_ftrs,beta3_ftrs,gamma_ftrs,high_ftrs,super_ftrs);
% ftrs = cat(1,raw_ftrs,beta1_ftrs,beta2_ftrs,beta3_ftrs,gamma_ftrs,high_ftrs);
% ftrs = cat(1,raw_ftrs,lowfreq_ftrs,alpha_ftrs,beta1_ftrs,beta2_ftrs);
end

