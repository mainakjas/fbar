function ftrs = sqiEogWithTheta_ftrs(sig,Fs)

% [N,Nchans] = size(sig);
signorm = bsxfun(@minus,sig,mean(sig));
% signorm = zscore(sig);

%% Frequency band features
% lowfreq Band features
lowfreq_f1 = 0.5;
lowfreq_f2 = 4;
[b,a] = butter(3,[fq2dec(lowfreq_f1,Fs),fq2dec(lowfreq_f2,Fs)]);
lowfreq_sig = filter(b,a,signorm);
lowfreq_pow = calc_fft(signorm,Fs,lowfreq_f1,lowfreq_f2);
    
lowfreq_mean_pow = mean(lowfreq_pow,1);
lowfreq_std_pow = std(lowfreq_pow,[],1);

lowfreq_max_amp = max(abs(lowfreq_sig),[],1);
lowfreq_std_amp = std(lowfreq_sig,[],1);
lowfreq_kurt = kurtosis(lowfreq_sig,[],1);
lowfreq_skew = skewness(lowfreq_sig,[],1);

lowfreq_ftrs = cat(1,lowfreq_mean_pow,lowfreq_std_pow,lowfreq_max_amp,lowfreq_std_amp,lowfreq_kurt,lowfreq_skew);

% Theta Band features
theta_f1 = 4;
theta_f2 = 8;
[b,a] = butter(3,[fq2dec(theta_f1,Fs),fq2dec(theta_f2,Fs)]);
theta_sig = filter(b,a,signorm);
theta_pow = calc_fft(signorm,Fs,theta_f1,theta_f2);
    
theta_mean_pow = mean(theta_pow,1);
theta_std_pow = std(theta_pow,[],1);

theta_max_amp = max(abs(theta_sig),[],1);
theta_std_amp = std(theta_sig,[],1);
theta_kurt = kurtosis(theta_sig,[],1);
theta_skew = skewness(theta_sig,[],1);

theta_ftrs = cat(1,theta_mean_pow,theta_std_pow,theta_max_amp,theta_std_amp,theta_kurt,theta_skew);

% Alpha Band features
alpha_f1 = 8;
alpha_f2 = 13;
[b,a] = butter(3,[fq2dec(alpha_f1,Fs),fq2dec(alpha_f2,Fs)]);
alpha_sig = filter(b,a,signorm);
alpha_pow = calc_fft(signorm,Fs,alpha_f1,alpha_f2);
    
alpha_mean_pow = mean(alpha_pow,1);
alpha_std_pow = std(alpha_pow,[],1);

alpha_max_amp = max(abs(alpha_sig),[],1);
alpha_std_amp = std(alpha_sig,[],1);
alpha_kurt = kurtosis(alpha_sig,[],1);
alpha_skew = skewness(alpha_sig,[],1);

alpha_ftrs = cat(1,alpha_mean_pow,alpha_std_pow,alpha_max_amp,alpha_std_amp,alpha_kurt,alpha_skew);

% Beta1 Band features
beta1_f1 = 13;
beta1_f2 = 18;
[b,a] = butter(3,[fq2dec(beta1_f1,Fs),fq2dec(beta1_f2,Fs)]);
beta1_sig = filter(b,a,signorm);
beta1_pow = calc_fft(signorm,Fs,beta1_f1,beta1_f2);
    
beta1_mean_pow = mean(beta1_pow,1);
beta1_std_pow = std(beta1_pow,[],1);

beta1_max_amp = max(abs(beta1_sig),[],1);
beta1_std_amp = std(beta1_sig,[],1);
beta1_kurt = kurtosis(beta1_sig,[],1);
beta1_skew = skewness(beta1_sig,[],1);

beta1_ftrs = cat(1,beta1_mean_pow,beta1_std_pow,beta1_max_amp,beta1_std_amp,beta1_kurt,beta1_skew);

% Beta2 Band features
beta2_f1 = 18;
beta2_f2 = 25;
[b,a] = butter(3,[fq2dec(beta2_f1,Fs),fq2dec(beta2_f2,Fs)]);
beta2_sig = filter(b,a,signorm);
beta2_pow = calc_fft(signorm,Fs,beta2_f1,beta2_f2);
    
beta2_mean_pow = mean(beta2_pow,1);
beta2_std_pow = std(beta2_pow,[],1);

beta2_max_amp = max(abs(beta2_sig),[],1);
beta2_std_amp = std(beta2_sig,[],1);
beta2_kurt = kurtosis(beta2_sig,[],1);
beta2_skew = skewness(beta2_sig,[],1);

beta2_ftrs = cat(1,beta2_mean_pow,beta2_std_pow,beta2_max_amp,beta2_std_amp,beta2_kurt,beta2_skew);


%% Combine all features
ftrs = cat(1,lowfreq_ftrs,theta_ftrs,alpha_ftrs,beta1_ftrs,beta2_ftrs);
end

