function saveResults2Text(fpath,fname,S,acc,opt)

%% Open/create file and read option fields and subfields
if ~exist([fpath,fname,'.txt'],'file')
    save([fpath,fname,'.txt'])
end
% scroll through opt fields to build headers
NamesL1 = fieldnames(opt);
NamesL2 = cell(length(NamesL1),1);
for i = 1:length(NamesL1)
    if isstruct(opt.(genvarname(NamesL1{i})))
        NamesL2{i} = fieldnames(opt.(genvarname(NamesL1{i})));
    end
end

fid = fopen([fpath,fname,'.txt'],'a');

%% Write Data
fprintf(fid,'\n\n-----------------------------------------------------------------------\n');
t = fix(clock);
fprintf(fid,'NEW RESULT: %s  %g:%g:%g\n',date,t(4),t(5),t(6));
fprintf(fid,'Options\n');
for i = 1:length(NamesL1)
    fprintf(fid,'\t%s:\n',NamesL1{i});
    fprintf(fid,'\t');
    for j = 1:length(NamesL2{i})
        x = opt.(genvarname(NamesL1{i})).(genvarname(NamesL2{i}{j}));
        if isnumeric(x)
            fprintf(fid,'\t%s(%g)',NamesL2{i}{j},x);
        elseif ischar(x)
            fprintf(fid,'\t%s(%s)',NamesL2{i}{j},x);
        end
    end
    fprintf(fid,'\n');
end
fprintf(fid,'Results\n');
for s = 1:length(S)
    fprintf(fid,'\t%s: %g (%g)\n',S{s},mean(acc(s,:)),std(acc(s,:)));
end
fprintf(fid,'\t%s: %g (%g)\n','AVG',mean(mean(acc(:,:),2)),mean(std(acc(:,:),[],2)));

%% Close file and exit with message
fclose(fid);
fprintf('Successfully wrote to file %s\n',[fpath,fname]);